This is a port of qtwebbrowser ( codename Roadtrip) for Ubports: Ubuntu Touch

Goals: 
- new working browser 
- update the qtwebbrowser code to qtquickcontrols2 - done, check for regressions
   * https://code.qt.io/cgit/qt-apps/qtwebbrowser.git/ is the original code to 
   compare to, keyboard actions were deleted, and anything that was a qtquickcontrols 1
   style was deleted - some of that may be causing the bugs so needs to be
   reworked as per: https://doc.qt.io/qt-5.11/qtquickcontrols2-differences.html
- Resize main window to keyboard and topbar
- scale better, remove either the home or tab button to put in bottom edge (my vote is home/favorites button, will center around bookmark centric navigiation, as tabs are limited)
- fix tabs (new tab button broken, but popups open in new tabs as expected)
- fix ssl error dialog
- Nice but not nessary: Can the virtual keyboard work? https://doc.qt.io/qt-5/qtvirtualkeyboard-deployment-guide.html
  maybe https://stackoverflow.com/questions/42576436/how-to-use-the-qtvirtualkeyboard
  didn't seem to compile with clickable - disable hunspell? does licnese allow use of virtual keyboard?
- Have fun!
- New name? or use code name.
