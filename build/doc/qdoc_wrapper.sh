#!/bin/sh
QT_VERSION=1.0.0
export QT_VERSION
QT_VER=1.0
export QT_VER
QT_VERSION_TAG=100
export QT_VERSION_TAG
QT_INSTALL_DOCS=/usr/share/qt5/doc
export QT_INSTALL_DOCS
BUILDDIR=/home/mateo/qtwebbrowser/build/doc
export BUILDDIR
exec /usr/lib/x86_64-linux-gnu/qt5/bin/qdoc "$@"
