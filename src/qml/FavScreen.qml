import QtQuick 2.5
import WebBrowser 1.0
import "assets"
import Ubuntu.Components 1.3

Rectangle {
    id: favScreen
    anchors.fill: parent

    NavigationBarFav {
        id: navigationFav

        anchors {
            left: parent.left
            right: parent.right
        }
    }        
        
    signal add(string title, string url, string iconUrl, string fallbackColor)
    onAdd: {
        if (listModel.count === gridViewMaxBookmarks) {
            navigation.refresh()
            messageBox.state = "full"
            state = "enabled"
            favScreen.forceActiveFocus()
            return
        }
        var icon = url.indexOf("qt.io") != -1 ? "assets/icons/qt.png" : iconUrl
        var element = { "title": title, "url": url, "iconUrl": icon, "fallbackColor": fallbackColor }
        listModel.append(element)
        set(listModel.count - 1)
    }

    signal remove(string url, int idx)
    onRemove: {
        var index = idx < 0 ? contains(url) : idx
        if (index < 0)
            return

        listModel.remove(index)
        gridView.forceLayout()
        navigation.refresh()
        if (!listModel.count)
            messageBox.state = "empty"
    }

    function get(index) {
        return listModel.get(index)
    }

    function contains(url) {
        for (var idx = 0; idx < listModel.count; ++idx) {
            if (listModel.get(idx).url === url)
                return idx;
        }
        return -1;
    }


    ListModel {
        id: listModel
        property string defaultBookmarks: "[{\"fallbackColor\":\"#46a2da\",\"iconUrl\":\"assets/icons/qt.png\",\"title\":\"Qt - Home\",\"url\":\"https://www.qt.io/\"},{\"fallbackColor\":\"#18394c\",\"iconUrl\":\"http://www.topgear.com/sites/all/themes/custom/tg/apple-touch-icon-144x144.png\",\"title\":\"Top Gear\",\"url\":\"http://www.topgear.com/\"},{\"fallbackColor\":\"#46a2da\",\"iconUrl\":\"https://duckduckgo.com/assets/icons/meta/DDG-iOS-icon_152x152.png\",\"title\":\"DuckDuckGo\",\"url\":\"https://duckduckgo.com/\"},{\"fallbackColor\":\"#ff8c0a\",\"iconUrl\":\"http://s.blogsmithmedia.com/www.engadget.com/assets/images/favicon-160x160.png\",\"title\":\"Engadget | Technology News, Advice and Features\",\"url\":\"http://www.engadget.com/\"},{\"fallbackColor\":\"#ff8c0a\",\"iconUrl\":\"https://www.openstreetmap.org/assets/favicon-194x194-32cdac24b02b88e09f0639bb92c760b2.png\",\"title\":\"OpenStreetMap\",\"url\":\"https://www.openstreetmap.org/\"},{\"fallbackColor\":\"#5caa15\",\"iconUrl\":\"http://www.redditstatic.com/icon.png\",\"title\":\"reddit: the front page of the internet\",\"url\":\"http://www.reddit.com/\"}]"

        Component.onCompleted: {
            listModel.clear()
            var string = AppEngine.restoreSetting("bookmarks", defaultBookmarks)
            if (!string)
                return
            var list = JSON.parse(string)
            for (var i = 0; i < list.length; ++i) {
                listModel.append(list[i])
            }
            navigation.refresh()
        }
        Component.onDestruction: {
            var list = []
            for (var i = 0; i < listModel.count; ++i) {
                list[i] = listModel.get(i)
            }
            AppEngine.saveSetting("bookmarks", JSON.stringify(list))
        }
    }
    
                    
        UbuntuListView {
                clip: true
                anchors {
                    top: navigationFav.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                model: listModel            

  
                delegate: ListItem {
                    height: units.gu(20)
                    
                    ListItemLayout {
                        height: units.gu(20) + (divider.visible ? divider.height : 0)
                        id: modelLayout2
                        title.text: model.title
                        title.color: "black"
                        title.textSize: Label.XLarge 
                        subtitle.text: model.url
                        subtitle.color: "#b1b1b1"
                        subtitle.textSize: Label.Large 

                        Rectangle{
                            SlotsLayout.position: SlotsLayout.Leading
                            width: units.gu(10)
                            height: units.gu(10)
                            color: "transparent"
                                
                            Icon {
                                width: parent.width
                                source: model.iconUrl
                            }
    
                        }

                        
                    }
                        divider.visible: false
                        onClicked: {
                            navigation.load(model.url)
                            bottomEdge.close()
                        }
                    
                leadingActions: ListItemActions {
                    actions: [
                        Action {
                            iconName: "delete"
                        onTriggered: {
                            var idx = favScreen.contains(model.url.toString())
                            if (idx !== -1) {
                                favScreen.remove("", idx)
                                return
                            }
                        }
                        }
                    ]
                }
                    }
            }
    

}
