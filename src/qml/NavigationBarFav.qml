import QtQuick 2.5
import QtQuick.Controls 2.0
//import QtQuick.Controls.Styles 2.0
import QtQuick.Layouts 1.0
import WebBrowser 1.0

import "assets"

ToolBar {
    id: root

    property alias addressBar: urlBarFav

    function contains(url) {
        for (var idx = 0; idx < listModel.count; ++idx) {
            if (listModel.get(idx).url === url)
                return idx;
        }
        return -1;
    }
    
        background: Rectangle {
            color: uiColor
            implicitHeight: toolBarSize + 3
        }


    RowLayout {
        height: toolBarSize
        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
        }
        spacing: 0

        UIButton {
            id: bookmarksButtonFav
            color: uiColor
            highlightColor: buttonPressedColor
            height: 54
            width: height
                anchors {
                    right: reloadButton.left
                    verticalCenter: addressBar.verticalCenter;
                }
            enabled: urlBarFav.text != "" && !settingsView.privateBrowsingEnabled
            source: favScreen.contains(navigation.webView.url.toString()) !== -1 ? "icons/Btn_Bookmark_Checked.png" : "icons/Btn_Bookmarks.png"
                visible: !cancelButton.visible
                
            onClicked: {
                if (!navigation.webView)
                    return
                var icon = "http://" + navigation.webView.url.toString().replace('http://','').replace('https://','').split(/[/?#]/)[0] + "/favicon.ico"
                   //navigation.webView.loading ? "" : navigation.webView.icon
                var idx = favScreen.contains(navigation.webView.url.toString())
                if (idx !== -1) {
                    favScreen.remove("", idx)
                    return
                }
                var count = favScreen.count
                favScreen.add(navigation.webView.title, navigation.webView.url, icon, AppEngine.fallbackColor())
                if (count < favScreen.count)
                    bookmarked = true
            }
        }
        Rectangle {
            width: 1
            height: parent.height
            color: uiSeparatorColor
        }
        Rectangle {
            Layout.fillWidth: true
            implicitWidth: 10
            height: parent.height
            color: uiColor
        }
        TextField {
            id: urlBarFav
            Layout.fillWidth: true
            text: navigation.addressBar.text
            placeholderText: qsTr("Search or type a URL")
           
            onTextChanged: favScreen.contains(urlBarFav.text)

            onActiveFocusChanged: {
                urlBarFav.focus = false
                bottomEdge.close()
            }
            
                font.family: defaultFontFamily
                font.pixelSize: 28
                selectionColor: uiHighlightColor
                selectedTextColor: "black"
                background: Rectangle {
                    implicitWidth: 514
                    implicitHeight: 56
                    border.color: settingsView.privateBrowsingEnabled ? "black" : textFieldStrokeColor
                    border.width: 1
                    radius: units.gu(2)
                    
                }
            

                  
                        
        }
        Rectangle {
            width: 1
            height: parent.height
            color: uiSeparatorColor
        }
        UIButton {
            id: settingsButton
            source: "icons/Btn_Settings.png"
            color: uiColor
            highlightColor: buttonPressedColor
            onClicked: {
                settingsView.state = "enabled"
            }
        }
    }

}
